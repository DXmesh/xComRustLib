// 文件是否存在的声明
#![allow(static_mut_refs)]
use crate::x_core::{add_request_handler, gen_id};
use std::{
    future::Future,
    sync::{Arc, Mutex},
    task::{Poll, Waker},
};
use x_common_lib::{
    base::status::Status,
    serial::{self, request_message::RequestMessage},
};
struct CommonApiStatus {
    is_finish: bool,
    status: Status,
    waker: Option<Waker>,
}

pub struct CommonStatusFuture {
    shared_state: Arc<Mutex<CommonApiStatus>>,
}

impl Future for CommonStatusFuture {
    type Output = Result<(), Status>;
    fn poll(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let mut shared_state = self.shared_state.lock().unwrap();

        if shared_state.is_finish {
            if shared_state.status.is_erorr() {
                let status = std::mem::take(&mut shared_state.status);
                return Poll::Ready(Err(status));
            } else {
                return Poll::Ready(Ok(()));
            }
        }
        shared_state.waker = Some(cx.waker().clone());
        Poll::Pending
    }
}

fn build_common_status_future() -> CommonStatusFuture {
    let shared_state = Arc::new(Mutex::new(CommonApiStatus {
        is_finish: false,
        status: Status::default(),
        waker: None,
    }));
    let future = CommonStatusFuture { shared_state };

    future
}

fn add_request_status_handler(request_id: i64, clone_shared_state: Arc<Mutex<CommonApiStatus>>) {
    add_request_handler(
        request_id,
        Box::new(move |buffer| {
            let waker = {
                let mut shared_state = clone_shared_state.lock().unwrap();

                shared_state
                    .status
                    .parse_from_bytes_return_num(buffer)
                    .unwrap();
                shared_state.is_finish = true;
                shared_state.waker.take()
            };

            if let Some(waker) = waker {
                waker.wake_by_ref();
            }
        }),
    );
}

struct ResultApiState<T> {
    is_finish: bool,
    status: Status,
    value: Option<T>,
    waker: Option<Waker>,
}

pub struct ResultApiFuture<T> {
    shared_state: Arc<Mutex<ResultApiState<T>>>,
}
//
impl<T: Send + Sync> Future for ResultApiFuture<T> {
    type Output = Result<T, Status>;
    fn poll(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let mut shared_state = self.shared_state.lock().unwrap();
        if shared_state.is_finish {
            if shared_state.status.is_erorr() {
                let status = std::mem::take(&mut shared_state.status);
                return Poll::Ready(Err(status));
            } else {
                let value = std::mem::take(&mut shared_state.value);
                return Poll::Ready(Ok(value.unwrap()));
            }
        }
        shared_state.waker = Some(cx.waker().clone());
        Poll::Pending
    }
}
//
pub struct ExistSharedState {
    is_finish: bool,
    is_exist: bool,
    waker: Option<Waker>,
}

pub struct ExistFuture {
    shared_state: Arc<Mutex<ExistSharedState>>,
}

impl Future for ExistFuture {
    type Output = bool;
    fn poll(self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        //
        let mut shared_state = self.shared_state.lock().unwrap();
        if shared_state.is_finish {
            return Poll::Ready(shared_state.is_exist);
        }
        shared_state.waker = Some(cx.waker().clone());
        Poll::Pending
    }
}

pub fn new_exist_future() -> ExistFuture {
    let shared_state = Arc::new(Mutex::new(ExistSharedState {
        is_finish: false,
        is_exist: false,
        waker: None,
    }));

    let future = ExistFuture { shared_state };

    future
}

// 添加文件或者文件夹是否存在
pub fn add_exist_request(shared_state: Arc<Mutex<ExistSharedState>>) -> i64 {
    let request_id = gen_id();
    let clone_shared_state = shared_state.clone();
    add_request_handler(
        request_id,
        Box::new(move |_buffer| {
            let waker = {
                let mut shared_state = clone_shared_state.lock().unwrap();
                //
                let is_exist = serial::read_bool(_buffer);
                shared_state.is_exist = is_exist;
                shared_state.is_finish = true;
                shared_state.waker.take()
            };
            if let Some(waker) = waker {
                waker.wake_by_ref();
            }
        }),
    );
    request_id
}

fn build_result_api_future<T>() -> ResultApiFuture<Box<T>>
where
    T: RequestMessage + Default + 'static,
{
    let shared_state = Arc::new(Mutex::new(ResultApiState::<Box<T>> {
        is_finish: false,
        status: Status::default(),
        waker: None,
        value: None,
    }));
    let future = ResultApiFuture::<Box<T>> { shared_state };

    future
}

fn add_request_result_handler<T>(
    request_id: i64,
    clone_shared_state: Arc<Mutex<ResultApiState<Box<T>>>>,
) where
    T: RequestMessage + Default + 'static,
{
    add_request_handler(
        request_id,
        Box::new(move |buffer| {
            let waker = {
                let mut shared_state = clone_shared_state.lock().unwrap();
                let read_bytes = shared_state
                    .status
                    .parse_from_bytes_return_num(buffer)
                    .unwrap();
                if read_bytes != buffer.len() as u64 {
                    let mut value = Box::new(T::default());
                    let bytes = &buffer[read_bytes as usize..];
                    value.parse_from_bytes_return_num(bytes).unwrap();
                    shared_state.value = Some(value);
                }
                shared_state.is_finish = true;
                shared_state.waker.take()
            };
            if let Some(waker) = waker {
                waker.wake_by_ref();
            }
        }),
    );
}

//

pub mod xport {
    use super::{
        add_exist_request, add_request_result_handler, add_request_status_handler,
        build_common_status_future, build_result_api_future, new_exist_future, CommonStatusFuture,
        ExistFuture, ResultApiFuture,
    };
    use crate::x_core::{
        gen_id, get_service_id, ADD_CHANNEL_ID_TO_REMOTE_SERVICE, BUILD_CHANNEL_API,
        CLOSE_CHANNEL_API, GET_ALL_CONN_ID, GET_ALL_INSTALLED_SERVICE, GET_ALL_LOCAL_SERVICE,
        GET_CHANNEL_ID_BY_CONN_ID, GET_NODE_PRIVATE_KEY, GET_SELF_CONN_ID, GET_SERVICE_DETAIL,
        GET_SERVICE_ID_API, HAS_SERVICE_API, IS_SELF_NODE, LOAD_SERVICE_API,
        REMOVE_REMOTE_SERVICE_ALL_CHANNEL_ID, SEND_MESSAGE_API, SET_CHANNEL_XRPC_PORT,
        UNLOAD_SERVICE_API,
    };

    use x_common_lib::serial::request_message::RequestMessage;
    use x_common_lib::service::sys_service_api::{
        BoolResponse, ChannelId, ConnIds, DXCDetailInfo, I64Response, LoadServiceRequest,
        ServiceInfo, ServiceInfos, ServiceKey, StringResponse, UnloadServiceRequest,
    };

    /**
     * 发送消息
     */
    pub fn send_message(
        request_id: i64,
        receiver_service_key: ServiceKey,
        channel_id: i64,
        buffer: Vec<u8>,
    ) {
        let receiver_buffer = receiver_service_key.serial().unwrap();
        unsafe {
            SEND_MESSAGE_API.assume_init_ref()(
                get_service_id(),
                request_id,
                receiver_buffer.as_ptr(),
                receiver_buffer.len() as u32,
                channel_id,
                buffer.as_ptr(),
                buffer.len() as u32,
            );
        }
    }

    pub fn load_service(param: &LoadServiceRequest) -> CommonStatusFuture {
        // 要先获取 dxc 的类型，然后再启动
        let future = build_common_status_future();
        //
        let request_id = gen_id();
        //
        let clone_shared_state = future.shared_state.clone();
        add_request_status_handler(request_id, clone_shared_state);
        let message = param.serial().unwrap();
        //
        unsafe {
            LOAD_SERVICE_API.assume_init_ref()(
                get_service_id(),
                request_id,
                message.as_ptr(),
                message.len() as u32,
            );
        };
        future
    }

    pub fn unload_service(param: &UnloadServiceRequest) -> CommonStatusFuture {
        // 也要先获取 dxc 的类型
        let future = build_common_status_future();
        let request_id = gen_id();
        let clone_shared_state = future.shared_state.clone();
        add_request_status_handler(request_id, clone_shared_state);
        let message = param.serial().unwrap();
        //
        unsafe {
            UNLOAD_SERVICE_API.assume_init_ref()(
                get_service_id(),
                request_id,
                message.as_ptr(),
                message.len() as u32,
            );
        }
        future
    }

    // 文件是否存在
    pub fn has_service(service_key: &ServiceKey) -> ExistFuture {
        let future = new_exist_future();

        let clone_shared_state = future.shared_state.clone();

        let request_id = add_exist_request(clone_shared_state);

        let message = service_key.serial().unwrap();
        //  调用 Api
        unsafe {
            HAS_SERVICE_API.assume_init_ref()(
                get_service_id(),
                request_id,
                message.as_ptr(),
                message.len() as u32,
            );
        };
        future
    }

    // 文件是否存在
    pub fn get_target_service_id(service_key: &ServiceKey) -> ResultApiFuture<Box<I64Response>> {
        let future = build_result_api_future::<I64Response>();
        let request_id = gen_id();
        let clone_shared_state = future.shared_state.clone();
        add_request_result_handler(request_id, clone_shared_state);
        let message = service_key.serial().unwrap();
        unsafe {
            GET_SERVICE_ID_API.assume_init_ref()(
                get_service_id(),
                request_id,
                message.as_ptr(),
                message.len() as u32,
            );
        };

        future
    }

    pub fn build_channel(node_id: i64) -> CommonStatusFuture {
        let future = build_common_status_future();
        unsafe {
            let request_id = gen_id();

            let clone_shared_state = future.shared_state.clone();
            add_request_status_handler(request_id, clone_shared_state);
            //
            BUILD_CHANNEL_API.assume_init_ref()(get_service_id(), request_id, node_id);
        }
        future
    }

    pub fn close_channel(channel_id: i64) -> CommonStatusFuture {
        let future = build_common_status_future();
        unsafe {
            let request_id = gen_id();

            let clone_shared_state = future.shared_state.clone();
            add_request_status_handler(request_id, clone_shared_state);
            //
            CLOSE_CHANNEL_API.assume_init_ref()(get_service_id(), request_id, channel_id);
        }
        future
    }

    pub fn get_all_local_service(filter_hide_node: bool) -> ResultApiFuture<Box<ServiceInfos>> {
        let future = build_result_api_future::<ServiceInfos>();

        let request_id = gen_id();

        let clone_shared_state = future.shared_state.clone();

        add_request_result_handler(request_id, clone_shared_state);

        unsafe {
            GET_ALL_LOCAL_SERVICE.assume_init_ref()(get_service_id(), request_id, filter_hide_node);
        }

        future
    }

    pub fn get_all_installed_service() -> ResultApiFuture<Box<ServiceInfos>> {
        let future = build_result_api_future::<ServiceInfos>();
        let request_id = gen_id();

        let clone_shared_state = future.shared_state.clone();

        add_request_result_handler(request_id, clone_shared_state);

        unsafe {
            GET_ALL_INSTALLED_SERVICE.assume_init_ref()(get_service_id(), request_id);
        }

        future
    }

    pub fn get_service_detail(service_key: &ServiceKey) -> ResultApiFuture<Box<DXCDetailInfo>> {
        let future = build_result_api_future::<DXCDetailInfo>();
        let request_id = gen_id();

        let clone_shared_state = future.shared_state.clone();

        add_request_result_handler(request_id, clone_shared_state);

        let message = service_key.serial().unwrap();
        //  调用 Api
        unsafe {
            GET_SERVICE_DETAIL.assume_init_ref()(
                get_service_id(),
                request_id,
                message.as_ptr(),
                message.len() as u32,
            );
        };

        future
    }

    pub fn add_channel_id_to_remote_services(
        channel_id: i64,
        remote_services: &ServiceInfos,
    ) -> CommonStatusFuture {
        let future = build_common_status_future();
        unsafe {
            let request_id = gen_id();

            let clone_shared_state = future.shared_state.clone();
            add_request_status_handler(request_id, clone_shared_state);
            //
            let message = remote_services.serial().unwrap();
            //
            ADD_CHANNEL_ID_TO_REMOTE_SERVICE.assume_init_ref()(
                get_service_id(),
                request_id,
                channel_id,
                message.as_ptr(),
                message.len() as u32,
            );
        }
        future
    }

    pub fn remove_remote_services_all_channel_id(
        channel_id: i64,
        remote_service: &ServiceInfo,
    ) -> CommonStatusFuture {
        let future = build_common_status_future();
        unsafe {
            let request_id = gen_id();

            let clone_shared_state = future.shared_state.clone();
            add_request_status_handler(request_id, clone_shared_state);
            //
            let message = remote_service.serial().unwrap();
            //
            REMOVE_REMOTE_SERVICE_ALL_CHANNEL_ID.assume_init_ref()(
                get_service_id(),
                request_id,
                channel_id,
                message.as_ptr(),
                message.len() as u32,
            );
        }
        future
    }

    pub fn set_channel_xrpc_port(
        channel_id: i64,
        xprc_port: u32,
    ) -> ResultApiFuture<Box<I64Response>> {
        let future = build_result_api_future::<I64Response>();
        unsafe {
            let request_id = gen_id();

            let clone_shared_state = future.shared_state.clone();
            add_request_result_handler(request_id, clone_shared_state);
            //
            SET_CHANNEL_XRPC_PORT.assume_init_ref()(
                get_service_id(),
                request_id,
                channel_id,
                xprc_port,
            );
        }
        future
    }
    //
    pub fn get_self_conn_id() -> i64 {
        let request_id = gen_id();
        unsafe { GET_SELF_CONN_ID.assume_init_ref()(get_service_id(), request_id) }
    }
    //

    pub fn get_all_conn_id() -> ResultApiFuture<Box<ConnIds>> {
        let future = build_result_api_future::<ConnIds>();
        //
        let request_id = gen_id();

        let clone_shared_state = future.shared_state.clone();
        add_request_result_handler(request_id, clone_shared_state);
        unsafe {
            GET_ALL_CONN_ID.assume_init_ref()(get_service_id(), request_id);
        }
        future
    }
    //
    pub fn get_channel_id_by_conn_id(conn_id: i64) -> ResultApiFuture<Box<ChannelId>> {
        let future = build_result_api_future::<ChannelId>();
        let request_id = gen_id();

        let clone_shared_state = future.shared_state.clone();
        add_request_result_handler(request_id, clone_shared_state);
        unsafe {
            GET_CHANNEL_ID_BY_CONN_ID.assume_init_ref()(get_service_id(), request_id, conn_id);
        }

        future
    }
    //
    pub fn get_node_private_key() -> ResultApiFuture<Box<StringResponse>> {
        let future = build_result_api_future::<StringResponse>();
        let request_id = gen_id();
        let clone_shared_state = future.shared_state.clone();
        add_request_result_handler(request_id, clone_shared_state);
        unsafe {
            GET_NODE_PRIVATE_KEY.assume_init_ref()(get_service_id(), request_id);
        }

        future
    }

    //
    pub fn is_self_node(conn_id: i64) -> ResultApiFuture<Box<BoolResponse>> {
        let future = build_result_api_future::<BoolResponse>();
        let request_id = gen_id();
        let clone_shared_state = future.shared_state.clone();
        add_request_result_handler(request_id, clone_shared_state);
        unsafe {
            IS_SELF_NODE.assume_init_ref()(get_service_id(), request_id, conn_id);
        }

        future
    }

    // pub fn get_config(table: &str) -> ResultApiFuture<StringResponse> {
    //     let future = build_result_api_future::<StringResponse>();
    //     let request_id = gen_id();
    //     let clone_shared_state = future.shared_state.clone();
    //     add_request_result_handler(request_id, clone_shared_state);
    //     let mut table_param = StringRequest::default();
    //     table_param.value = table.to_owned();

    //     let message = table_param.serial().unwrap();
    //     unsafe {
    //         GET_CONFIG.assume_init_ref()(
    //             get_service_id(),
    //             request_id,
    //             message.as_ptr(),
    //             message.len() as u32,
    //         );
    //     }

    //     future
    // }

    pub mod event {
        use std::{
            collections::HashMap,
            ptr::null,
            slice,
            sync::{Mutex, RwLock},
        };

        use lazy_static::lazy_static;
        use protobuf::CodedOutputStream;
        use x_common_lib::{
            base::{
                dll_api::event::{
                    CHANNEL_CONNECTED, CHANNEL_DISCONNECTED, IPC_SERVICE_DISCONNECT,
                    LOCAL_SERVICE_OFF, LOCAL_SERVICE_ON, LOOK_FOR_DXC, MESSAGE_IN,
                },
                status::Status,
            },
            serial::request_message::RequestMessage,
            service::sys_service_api::{ChannelEvent, ServiceInfo, ServiceKey, VerifyInfo},
        };

        use crate::x_core::{EVENT_RESPONSE_FUNC_API, SUBSCRIBE, UNSUBSCRIBE};

        lazy_static! {
          static ref SUBSCRIBERIDMAP:Mutex<HashMap<u16, i64>> = Mutex::new(HashMap::new());
          // 订阅的map
          static ref EVENT_SUBSCRIBER_MAP: RwLock<HashMap<u16, Box<dyn Fn(i64, *const u8,  u32) ->bool + Send + Sync>>> = RwLock::new(HashMap::new());

        }

        pub struct EventContext {
            publish_id: i64,
            is_resp: bool,
        }

        impl EventContext {
            pub fn new(publish_id: i64) -> Self {
                EventContext {
                    publish_id,
                    is_resp: false,
                }
            }

            pub fn resp_error<S: AsRef<str>>(mut self, err_msg: S) {
                let status = Status::error(err_msg.as_ref().into());

                let buffer = status.compute_size_with_tag_and_len_return_buffer();
                unsafe {
                    EVENT_RESPONSE_FUNC_API.assume_init_ref()(
                        self.publish_id,
                        buffer.as_ptr(),
                        buffer.len() as u32,
                    );
                }
                self.is_resp = true;
            }

            pub fn resp<T>(mut self, t: T)
            where
                T: RequestMessage,
            {
                let status = Status::default();
                let mut size = 0;
                size += status.compute_size_with_tag_and_len();
                size += t.compute_size_with_tag_and_len();
                let mut buffer = Vec::with_capacity(size as usize);
                unsafe {
                    buffer.set_len(size as usize);
                }
                {
                    let mut os = CodedOutputStream::bytes(&mut buffer);
                    status.serial_with_tag_and_len(&mut os);
                    t.serial_with_tag_and_len(&mut os);
                }

                unsafe {
                    EVENT_RESPONSE_FUNC_API.assume_init_ref()(
                        self.publish_id,
                        buffer.as_ptr(),
                        buffer.len() as u32,
                    );
                }
                self.is_resp = true;
            }
        }

        impl Drop for EventContext {
            fn drop(&mut self) {
                if self.is_resp {
                    return;
                }
                unsafe {
                    EVENT_RESPONSE_FUNC_API.assume_init_ref()(self.publish_id, null(), 0);
                }
            }
        }

        pub extern "C" fn event_receiver(
            event_id: u16,
            publish_id: i64,
            buffer: *const u8,
            buffer_len: u32,
        ) -> bool {
            let event_subscriber_map = EVENT_SUBSCRIBER_MAP.read().unwrap();
            let hanlder = event_subscriber_map.get(&event_id);
            if hanlder.is_none() {
                return false;
            }
            let hanlder = hanlder.unwrap();
            // 调用的是各个时间的处理函数
            hanlder(publish_id, buffer, buffer_len)
        }
        //
        pub fn subscribe_channel_connected(
            handler: Box<dyn Fn(EventContext, Box<ChannelEvent>) + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();

            //
            event_subscriber_map.insert(
                CHANNEL_CONNECTED,
                Box::new(move |publish_id, buffer, buffer_len| {
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut channel_event = Box::new(ChannelEvent::default());
                    channel_event.parse_from_bytes(vec_buffer).unwrap();

                    let ctx = EventContext::new(publish_id);
                    handler(ctx, channel_event);
                    true
                }),
            );
            unsafe {
                let subscriber_id =
                    SUBSCRIBE.assume_init_ref()(CHANNEL_CONNECTED, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(CHANNEL_CONNECTED, subscriber_id);
            }
        }
        //
        pub fn subscribe_channel_disconnected(
            handler: Box<dyn Fn(EventContext, Box<ChannelEvent>) + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();
            event_subscriber_map.insert(
                CHANNEL_DISCONNECTED,
                Box::new(move |publish_id, buffer, buffer_len| {
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut channel_event = Box::new(ChannelEvent::default());
                    channel_event.parse_from_bytes(vec_buffer).unwrap();

                    let ctx = EventContext::new(publish_id);
                    handler(ctx, channel_event);
                    true
                }),
            );
            unsafe {
                let subscriber_id =
                    SUBSCRIBE.assume_init_ref()(CHANNEL_DISCONNECTED, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(CHANNEL_DISCONNECTED, subscriber_id);
            }
        }

        pub fn subscribe_look_for_dxc(
            handler: Box<dyn Fn(EventContext, Box<ServiceKey>) + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();
            event_subscriber_map.insert(
                LOOK_FOR_DXC,
                Box::new(move |publish_id, buffer, buffer_len| {
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut service_key = Box::new(ServiceKey::default());
                    service_key.parse_from_bytes(vec_buffer).unwrap();

                    let ctx = EventContext::new(publish_id);
                    handler(ctx, service_key);
                    true
                }),
            );
            unsafe {
                let subscriber_id = SUBSCRIBE.assume_init_ref()(LOOK_FOR_DXC, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(LOOK_FOR_DXC, subscriber_id);
            }
        }

        //
        pub fn subscribe_local_service_on(
            handler: Box<dyn Fn(EventContext, Box<ServiceInfo>) + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();
            event_subscriber_map.insert(
                LOCAL_SERVICE_ON,
                Box::new(move |publish_id, buffer, buffer_len| {
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut service_infos = Box::new(ServiceInfo::default());
                    service_infos.parse_from_bytes(vec_buffer).unwrap();

                    let ctx = EventContext::new(publish_id);

                    handler(ctx, service_infos);

                    true
                }),
            );
            unsafe {
                let subscriber_id =
                    SUBSCRIBE.assume_init_ref()(LOCAL_SERVICE_ON, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(LOCAL_SERVICE_ON, subscriber_id);
            }
        }
        //
        pub fn subscribe_local_service_off(
            handler: Box<dyn Fn(EventContext, Box<ServiceInfo>) + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();
            event_subscriber_map.insert(
                LOCAL_SERVICE_OFF,
                Box::new(move |publish_id, buffer, buffer_len| {
                    //
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut service_infos = Box::new(ServiceInfo::default());
                    service_infos.parse_from_bytes(vec_buffer).unwrap();

                    let ctx = EventContext::new(publish_id);
                    handler(ctx, service_infos);
                    true
                }),
            );
            unsafe {
                let subscriber_id =
                    SUBSCRIBE.assume_init_ref()(LOCAL_SERVICE_OFF, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(LOCAL_SERVICE_OFF, subscriber_id);
            }
        }

        pub fn subscribe_ipc_disconnected(
            handler: Box<dyn Fn(EventContext, Box<ServiceInfo>) + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();
            event_subscriber_map.insert(
                IPC_SERVICE_DISCONNECT,
                Box::new(move |publish_id, buffer, buffer_len| {
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut service_infos = Box::new(ServiceInfo::default());
                    service_infos.parse_from_bytes(vec_buffer).unwrap();

                    let ctx = EventContext::new(publish_id);

                    handler(ctx, service_infos);

                    true
                }),
            );
            unsafe {
                let subscriber_id =
                    SUBSCRIBE.assume_init_ref()(IPC_SERVICE_DISCONNECT, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(IPC_SERVICE_DISCONNECT, subscriber_id);
            }
        }

        //
        pub fn subscribe_message_in(
            handler: Box<dyn Fn(EventContext, Box<VerifyInfo>) -> bool + Send + Sync>,
        ) {
            //
            let mut event_subscriber_map = EVENT_SUBSCRIBER_MAP.write().unwrap();
            event_subscriber_map.insert(
                MESSAGE_IN,
                Box::new(move |publish_id, buffer, buffer_len| {
                    let vec_buffer =
                        unsafe { slice::from_raw_parts(buffer as *mut u8, buffer_len as usize) };
                    let mut verify_info = Box::new(VerifyInfo::default());
                    verify_info.parse_from_bytes(vec_buffer).unwrap();
                    let ctx = EventContext::new(publish_id);
                    handler(ctx, verify_info)
                }),
            );
            unsafe {
                let subscriber_id = SUBSCRIBE.assume_init_ref()(MESSAGE_IN, 0, event_receiver);
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();
                subscriber_map.insert(MESSAGE_IN, subscriber_id);
            }
        }

        pub fn unsubscribe_all() {
            unsafe {
                let mut subscriber_map = SUBSCRIBERIDMAP.lock().unwrap();

                for (event_id, subscriber_id) in subscriber_map.iter() {
                    UNSUBSCRIBE.assume_init_ref()(*event_id, *subscriber_id);
                }

                subscriber_map.clear();
            }
        }
    }

    pub mod stream {
        use crate::{
            x_api::{add_request_status_handler, build_common_status_future, CommonStatusFuture},
            x_core::{
                gen_id, get_service_id, DXC_CANCEL_SUBSCRIBE_TOPIC, DXC_SUBSCRIBE_TOPIC,
                PULISH_MESSAGE, XPORT_CANCEL_SUBSCRIBE_TOPIC, XPORT_SUBSCRIBE_TOPIC,
            },
        };
        use x_common_lib::{
            serial::request_message::RequestMessage,
            service::sys_service_api::{PublishInfo, SubscribeTopicInfo, TopicKey},
        };
        pub fn publish_message<T>(publish_info: PublishInfo, msg: &Box<T>)
        where
            T: RequestMessage + Default + 'static,
        {
            let mut topic_key = TopicKey::default();

            topic_key.topic = publish_info.topic.clone();
            topic_key.tag = publish_info.tag.clone();

            //
            let mut size = topic_key.compute_size_with_tag_and_len();
            size += msg.compute_size();
            let mut buffer: Vec<u8> = Vec::with_capacity(size as usize);
            //
            let mut os = protobuf::CodedOutputStream::vec(&mut buffer);
            topic_key.serial_with_tag_and_len(&mut os);
            msg.serial_with_output_stream(&mut os).unwrap();
            os.flush().unwrap();
            drop(os);
            //

            let publish_info_buffer = publish_info.serial().unwrap();
            unsafe {
                //
                PULISH_MESSAGE.assume_init_ref()(
                    publish_info_buffer.as_ptr(),
                    publish_info_buffer.len() as u32,
                    buffer.as_ptr(),
                    buffer.len() as u32,
                );
            }
        }

        pub fn publish_empty_message(publish_info: PublishInfo) {
            let mut topic_key = TopicKey::default();

            topic_key.topic = publish_info.topic.clone();
            topic_key.tag = publish_info.tag.clone();

            let size = topic_key.compute_size_with_tag_and_len();
            let mut buffer: Vec<u8> = Vec::with_capacity(size as usize);

            let mut os = protobuf::CodedOutputStream::vec(&mut buffer);
            topic_key.serial_with_tag_and_len(&mut os);
            os.flush().unwrap();
            drop(os);

            // let size = protobuf::rt::string_size(1, &publish_info.topic);
            // let mut buffer: Vec<u8> = Vec::with_capacity(size as usize);
            // //
            // let mut os = protobuf::CodedOutputStream::vec(&mut buffer);
            // os.write_string(1, &publish_info.topic).unwrap();

            // os.flush().unwrap();
            // drop(os);
            // //

            let publish_info_buffer = publish_info.serial().unwrap();
            unsafe {
                //
                PULISH_MESSAGE.assume_init_ref()(
                    publish_info_buffer.as_ptr(),
                    publish_info_buffer.len() as u32,
                    buffer.as_ptr(),
                    buffer.len() as u32,
                );
            }
        }

        pub fn xport_subscribe_topic(subscribe_info: SubscribeTopicInfo) -> CommonStatusFuture {
            let future = build_common_status_future();
            unsafe {
                let request_id = gen_id();

                let clone_shared_state = future.shared_state.clone();
                add_request_status_handler(request_id, clone_shared_state);
                //
                let message = subscribe_info.serial().unwrap();

                XPORT_SUBSCRIBE_TOPIC.assume_init_ref()(
                    get_service_id(),
                    request_id,
                    message.as_ptr(),
                    message.len() as u32,
                );
            }
            future
        }

        pub fn xport_cancel_subscribe_topic(
            subscribe_info: SubscribeTopicInfo,
        ) -> CommonStatusFuture {
            let future = build_common_status_future();
            unsafe {
                let request_id = gen_id();

                let clone_shared_state = future.shared_state.clone();
                add_request_status_handler(request_id, clone_shared_state);
                //
                let message = subscribe_info.serial().unwrap();

                XPORT_CANCEL_SUBSCRIBE_TOPIC.assume_init_ref()(
                    get_service_id(),
                    request_id,
                    message.as_ptr(),
                    message.len() as u32,
                );
            }
            future
        }

        pub fn dxc_subscribe_topic(mut subscribe_info: SubscribeTopicInfo) -> CommonStatusFuture {
            let future = build_common_status_future();
            unsafe {
                let request_id = gen_id();

                let clone_shared_state = future.shared_state.clone();
                add_request_status_handler(request_id, clone_shared_state);
                //
                subscribe_info.service_id = get_service_id();
                //
                let message = subscribe_info.serial().unwrap();

                DXC_SUBSCRIBE_TOPIC.assume_init_ref()(
                    get_service_id(),
                    request_id,
                    message.as_ptr(),
                    message.len() as u32,
                );
            }
            future
        }

        pub fn dxc_cancel_subscribe_topic(
            subscribe_info: SubscribeTopicInfo,
        ) -> CommonStatusFuture {
            let future = build_common_status_future();
            unsafe {
                let request_id = gen_id();

                let clone_shared_state = future.shared_state.clone();
                add_request_status_handler(request_id, clone_shared_state);
                //
                let message = subscribe_info.serial().unwrap();

                DXC_CANCEL_SUBSCRIBE_TOPIC.assume_init_ref()(
                    get_service_id(),
                    request_id,
                    message.as_ptr(),
                    message.len() as u32,
                );
            }
            future
        }
    }
    //
}
