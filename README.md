## xCom Library

xCom Library 是 DXMesh 应用组件 DXC(DXMesh Componet)，开发库。将 xPort 注入的接口，封装成 Rust 的 async 接口, 提供 async 运行时。

## DXC 构建方法

下载 [xCom Builder](https://gitee.com/DXmesh/xComBuilder/releases) 生成器工程


### 设置 xBuilder 

  下载之后，将 xBuilder 设置到全局遍历中


### 创建工程

  在统计目录下，创建任一目录，以工程名命名的目录（必须为英文）, 如以下例子:

  ```
  mkdir demo1 # 常见目录
  cd demo1 # 进入目录
  xBuilder new -n Demo1 # 创建工程
  xBuilder init # 初始化工程
  ``` 
  
  会生成以下文件：

  ```
  │  Cargo.toml
  │
  ├─protos
  │      source-api-0.0.1.proto
  │
  └─src
      │  lib.rs
      │  service.rs
      │
      └─x_com
              import_api.rs
              mod.rs
              source_api.rs
              xport_core.rs
  ```

#### 目录说明

- protos
  
  - source-api-0.0.1.proto 为工程接口的描述文件

- src/x_com

  - import_api.rs 调用其他 DXC 接口的代码
  - source_api.rs 自身接口所用结构体序列化/反序列化代码
  - xport_core.rs 导出 DXC 组件初始化/结束/以及消息分发的函数
  
- src/service.rs
  
  该文件根据 source-api-0.0.1.proto 生成的 接口文件，所有的逻辑代码都在此开发。
